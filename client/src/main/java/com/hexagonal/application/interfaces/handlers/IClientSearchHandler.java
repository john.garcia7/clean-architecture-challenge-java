package com.hexagonal.application.interfaces.handlers;

import com.hexagonal.application.dto.ClientDto;
import com.hexagonal.application.dto.SearchDto;
import com.hexagonal.domain.pojos.GenericResultsPage;

public interface IClientSearchHandler {
    GenericResultsPage<ClientDto> search(SearchDto searchDto);
}
