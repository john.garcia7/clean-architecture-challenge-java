package com.hexagonal.application.interfaces.handlers;

import com.hexagonal.application.dto.ClientDto;

public interface IUpdateClientHandler {
    ClientDto update(Long id, ClientDto clientDto);
}
