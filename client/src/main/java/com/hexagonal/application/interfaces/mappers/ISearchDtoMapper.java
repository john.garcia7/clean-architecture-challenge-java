package com.hexagonal.application.interfaces.mappers;

import com.hexagonal.application.dto.SearchDto;
import com.hexagonal.domain.pojos.SearchInfo;

public interface ISearchDtoMapper {
    
    SearchInfo toSearchInfo(SearchDto searchDto);

}
