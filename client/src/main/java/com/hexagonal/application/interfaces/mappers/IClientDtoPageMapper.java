package com.hexagonal.application.interfaces.mappers;

import java.util.Map;

import com.hexagonal.application.dto.ClientDto;
import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.pojos.GenericResultsPage;

public interface IClientDtoPageMapper {
    
    GenericResultsPage<ClientDto> toClientDtoPage(GenericResultsPage<Client> clientsPage, Map<Long, Image> imagesDicMap);

}
