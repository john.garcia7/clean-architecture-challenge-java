package com.hexagonal.application.interfaces.handlers;

import com.hexagonal.application.dto.ClientDto;

public interface IAddClientHandler {
    ClientDto add(ClientDto clientDto);
}
