package com.hexagonal.application.interfaces.handlers;

import com.hexagonal.application.dto.ClientDto;

public interface IGetClientHandler {
    ClientDto get(Long id);
}
