package com.hexagonal.application.interfaces.mappers;

import com.hexagonal.application.dto.ClientDto;
import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.entites.Image;

public interface IClientDtoMapper {
    ClientDto toClientDto(Client client, Image image);

    public Client toClient(ClientDto clientDto);
}
