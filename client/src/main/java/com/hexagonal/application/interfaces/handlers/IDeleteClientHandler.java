package com.hexagonal.application.interfaces.handlers;

public interface IDeleteClientHandler {
    void delete(Long id);
}
