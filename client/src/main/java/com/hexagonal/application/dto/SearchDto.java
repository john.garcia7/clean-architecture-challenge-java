package com.hexagonal.application.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hexagonal.domain.pojos.PageInfo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SearchDto {
    @JsonProperty("searchFilter")
    private SearchFilterDto searchFilterDto;
    private PageInfo pageInfo;
}
