package com.hexagonal.application.handlers;

import org.springframework.stereotype.Service;

import com.hexagonal.application.dto.ClientDto;
import com.hexagonal.application.interfaces.handlers.IAddClientHandler;
import com.hexagonal.application.interfaces.mappers.IClientDtoMapper;
import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.ports.input.client.IAddClientService;
import com.hexagonal.domain.ports.input.image.IAddImageService;

@Service
public class AddClientHandler implements IAddClientHandler {
    private final IAddClientService addClientService;
    private final IClientDtoMapper clientDtoMapper;
    private final IAddImageService addImageService;

    public AddClientHandler(
        IAddClientService addClientService,
        IClientDtoMapper clientDtoMapper,
        IAddImageService addImageService) {
        this.addClientService = addClientService;
        this.clientDtoMapper = clientDtoMapper;
        this.addImageService = addImageService;
    }

    @Override
    public ClientDto add(ClientDto clientDto) {
        Client client = addClientService.add(clientDtoMapper.toClient(clientDto));
        
        Image image = new Image();
        image.setPersonId(client.getId());
        image.setImageData(clientDto.getPhoto());
        
        addImageService.add(image);

        return clientDtoMapper.toClientDto(client, image);
    }
}
