package com.hexagonal.application.handlers;

import org.springframework.stereotype.Service;

import com.hexagonal.application.interfaces.handlers.IDeleteClientHandler;
import com.hexagonal.domain.ports.input.client.IDeleteClientService;
import com.hexagonal.domain.ports.input.image.IDeleteImageService;

@Service
public class DeleteClientHandler implements IDeleteClientHandler {
    private final IDeleteClientService deleteClientService;
    private final IDeleteImageService deleteImageService;

    public DeleteClientHandler(
        IDeleteClientService deleteClientService,
        IDeleteImageService deleteImageService) {
        this.deleteClientService = deleteClientService;
        this.deleteImageService = deleteImageService;
    }

    @Override
    public void delete(Long id) {
        deleteClientService.delete(id);
        deleteImageService.delete(id);
    }
}
