package com.hexagonal.application.handlers;

import org.springframework.stereotype.Service;

import com.hexagonal.application.dto.ClientDto;
import com.hexagonal.application.interfaces.handlers.IUpdateClientHandler;
import com.hexagonal.application.interfaces.mappers.IClientDtoMapper;
import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.ports.input.client.IUpdateClientService;
import com.hexagonal.domain.ports.input.image.IUpdateImageService;

@Service
public class UpdateClientHandler implements IUpdateClientHandler {
    private final IUpdateClientService updateClientService;
    private final IClientDtoMapper clientDtoMapper;
    private final IUpdateImageService updateImageService;

    public UpdateClientHandler(
        IUpdateClientService updateClientService,
        IClientDtoMapper clientDtoMapper,
        IUpdateImageService updateImageService) {
        this.updateClientService = updateClientService;
        this.clientDtoMapper = clientDtoMapper;
        this.updateImageService = updateImageService;
    }

    @Override
    public ClientDto update(Long id, ClientDto clientDto) {
        Client client = updateClientService.update(clientDtoMapper.toClient(clientDto));
        client.setId(id);

        Image image = new Image();
        image.setPersonId(client.getId());
        image.setImageData(clientDto.getPhoto());
        
        updateImageService.update(image);

        return clientDtoMapper.toClientDto(client, image);
    }
}
