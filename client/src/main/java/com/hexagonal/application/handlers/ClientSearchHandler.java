package com.hexagonal.application.handlers;

import org.springframework.stereotype.Service;

import com.hexagonal.application.dto.ClientDto;
import com.hexagonal.application.dto.SearchDto;
import com.hexagonal.application.interfaces.handlers.IClientSearchHandler;
import com.hexagonal.application.interfaces.mappers.IClientDtoPageMapper;
import com.hexagonal.application.interfaces.mappers.ISearchDtoMapper;
import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.pojos.GenericResultsPage;
import com.hexagonal.domain.pojos.SearchInfo;
import com.hexagonal.domain.ports.input.client.IClientSearchService;
import com.hexagonal.domain.ports.input.image.IGetImageListService;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ClientSearchHandler implements IClientSearchHandler {
    private final IClientSearchService clientSearchService;
    private final IClientDtoPageMapper clientDtoPageMapper;
    private final IGetImageListService getImageListService;
    private final ISearchDtoMapper searchDtoMapper;

    public ClientSearchHandler(
        IClientSearchService clientSearchService,
        IClientDtoPageMapper clientDtoPageMapper,
        IGetImageListService getImageListService,
        ISearchDtoMapper searchDtoMapper) {
        this.clientSearchService = clientSearchService;
        this.clientDtoPageMapper = clientDtoPageMapper;
        this.getImageListService = getImageListService;
        this.searchDtoMapper = searchDtoMapper;
    }

    @Override
    public GenericResultsPage<ClientDto> search(SearchDto searchDto) {
        SearchInfo searchInfo = searchDtoMapper.toSearchInfo(searchDto);
        GenericResultsPage<Client> clientsPage = clientSearchService.search(searchInfo);

        List<Long> clientsIdList = clientsPage.getResults().stream().map(Client::getId).collect(Collectors.toList());
        Map<Long, Image> imagesDicMap = getImageListService.getByClientIdList(clientsIdList);

        return clientDtoPageMapper.toClientDtoPage(clientsPage, imagesDicMap);
    }
}
