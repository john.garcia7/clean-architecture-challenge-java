package com.hexagonal.application.handlers;

import org.springframework.stereotype.Service;

import com.hexagonal.application.dto.ClientDto;
import com.hexagonal.application.interfaces.handlers.IGetClientHandler;
import com.hexagonal.application.interfaces.mappers.IClientDtoMapper;
import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.ports.input.client.IGetClientService;
import com.hexagonal.domain.ports.input.image.IGetImageService;

@Service
public class GetClientHandler implements IGetClientHandler {
    private final IGetClientService getClientService;
    private final IClientDtoMapper clientDtoMapper;
    private final IGetImageService getImageService;

    public GetClientHandler(
        IGetClientService getClientService,
        IClientDtoMapper clientDtoMapper,
        IGetImageService getImageService) {
        this.getClientService = getClientService;
        this.clientDtoMapper = clientDtoMapper;
        this.getImageService = getImageService;
    }

    @Override
    public ClientDto get(Long id) {
        Image image = getImageService.get(id);

        return clientDtoMapper.toClientDto(getClientService.get(id), image);
    }
}
