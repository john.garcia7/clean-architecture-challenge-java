package com.hexagonal.application.mappers;

import org.springframework.stereotype.Service;

import com.hexagonal.application.dto.ClientDto;
import com.hexagonal.application.interfaces.mappers.IClientDtoMapper;
import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.Enums;

@Service
public class ClientDtoMapper implements IClientDtoMapper {
    @Override
    public ClientDto toClientDto(Client client, Image image) {
        return new ClientDto(
            client.getId(),
            client.getFirstName(),
            client.getLastName(),
            client.getIdType().toString(),
            client.getIdNumber(),
            client.getAge(),
            client.getCityOfBirth(),
            image.getImageData()
        );
    }

    @Override
    public Client toClient(ClientDto clientDto) {
        Client client = new Client();
        client.setId(clientDto.getId());
        client.setFirstName(clientDto.getFirstName());
        client.setLastName(clientDto.getLastName());
        client.setIdType(Enums.IdentificationType.valueOf(clientDto.getIdType()));
        client.setIdNumber(clientDto.getIdNumber());
        client.setAge(clientDto.getAge());
        client.setCityOfBirth(clientDto.getCityOfBirth());

        return client;
    }
}
