package com.hexagonal.application.mappers;

import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.hexagonal.application.dto.ClientDto;
import com.hexagonal.application.interfaces.mappers.IClientDtoMapper;
import com.hexagonal.application.interfaces.mappers.IClientDtoPageMapper;
import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.pojos.GenericResultsPage;

@Service
public class ClientDtoPageMapper implements IClientDtoPageMapper {

    private final IClientDtoMapper clientDtoMapper;

    public ClientDtoPageMapper(
        IClientDtoMapper clientDtoMapper) {
        this.clientDtoMapper = clientDtoMapper;
    }
    
    public GenericResultsPage<ClientDto> toClientDtoPage(GenericResultsPage<Client> clientsPage, Map<Long, Image> imagesDicMap) {
        GenericResultsPage<ClientDto> responsePage = new GenericResultsPage<>();
        responsePage.setPageInfo(clientsPage.getPageInfo());
        responsePage.setTotalPages(clientsPage.getTotalPages());
        responsePage.setTotalResults(clientsPage.getTotalResults());
        responsePage.setResults(clientsPage.getResults().stream().map(client -> 
            { 
                Image image = imagesDicMap.get(client.getId());

                return clientDtoMapper.toClientDto(client, image);
            }).collect(Collectors.toList()));

        return responsePage;
    }

}
