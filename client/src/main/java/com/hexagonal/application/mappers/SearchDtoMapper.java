package com.hexagonal.application.mappers;

import org.springframework.stereotype.Service;

import com.hexagonal.application.dto.SearchDto;
import com.hexagonal.application.interfaces.mappers.ISearchDtoMapper;
import com.hexagonal.domain.Enums;
import com.hexagonal.domain.pojos.SearchFilter;
import com.hexagonal.domain.pojos.SearchInfo;

@Service
public class SearchDtoMapper implements ISearchDtoMapper {
    
    public SearchInfo toSearchInfo(SearchDto searchDto) {
        SearchInfo searchInfo = new SearchInfo();
        searchInfo.setPageInfo(searchDto.getPageInfo());

        SearchFilter searchFilter = null;

        if (searchDto.getSearchFilterDto() != null) {
            searchFilter = new SearchFilter();
            searchFilter.setAge(searchDto.getSearchFilterDto().getAge());
            searchFilter.setIdType(searchDto.getSearchFilterDto().getIdType() != null ? 
                Enums.IdentificationType.valueOf(searchDto.getSearchFilterDto().getIdType()).getIdTypeValue() : null);
            searchFilter.setIdNumber(searchDto.getSearchFilterDto().getIdNumber());
        }

        searchInfo.setSearchFilter(searchFilter);

        return searchInfo;
    }

}
