package com.hexagonal.domain.services.client;

import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.exceptions.DuplicatedItemException;
import com.hexagonal.domain.ports.input.client.IAddClientService;
import com.hexagonal.domain.ports.output.IClientPersistance;

public class AddClientService implements IAddClientService {

    private final IClientPersistance clientPersistance;
    
    public AddClientService(IClientPersistance clientPersistence) {
        this.clientPersistance = clientPersistence;
    }    
    
    @Override
    public Client add(Client client) throws DuplicatedItemException {
        Client existingClient = clientPersistance.getByIdentification(client.getIdType().getIdTypeValue(), client.getIdNumber());
        if(existingClient != null) {
            throw new DuplicatedItemException("A client with the same identification type and identification number already exists.");
        }
        
        return clientPersistance.add(client);
    }
}
