package com.hexagonal.domain.services.image;

import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.exceptions.DuplicatedItemException;
import com.hexagonal.domain.ports.input.image.IAddImageService;
import com.hexagonal.domain.ports.output.IImagePersistance;

public class AddImageService implements IAddImageService {
    
    private final IImagePersistance imagePersistance;

    public AddImageService(IImagePersistance imagePersistance) {
        this.imagePersistance = imagePersistance;
    }

    public void add(Image image) {
        if(imagePersistance.get(image.getPersonId()) != null) {
            throw new DuplicatedItemException("An image currently exists for the client.");
        }

        imagePersistance.add(image);
    }
}
