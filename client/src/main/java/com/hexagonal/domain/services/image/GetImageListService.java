package com.hexagonal.domain.services.image;

import java.util.List;
import java.util.Map;

import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.ports.input.image.IGetImageListService;
import com.hexagonal.domain.ports.output.IImagePersistance;

public class GetImageListService implements IGetImageListService {
    private final IImagePersistance imagePersistance;

    public GetImageListService(IImagePersistance imagePersistance) {
        this.imagePersistance = imagePersistance;
    }

    public Map<Long, Image> getByClientIdList(List<Long> personIdList) {
        return imagePersistance.getByClientIdList(personIdList);
    }
}
