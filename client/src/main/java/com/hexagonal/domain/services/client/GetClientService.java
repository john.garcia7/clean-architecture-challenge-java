package com.hexagonal.domain.services.client;

import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.exceptions.InvalidIdentifierException;
import com.hexagonal.domain.exceptions.ItemNotFoundException;
import com.hexagonal.domain.ports.input.client.IGetClientService;
import com.hexagonal.domain.ports.output.IClientPersistance;

public class GetClientService implements IGetClientService {
    private final IClientPersistance clientPersistance;
    
    public GetClientService(IClientPersistance clientPersistance) {
        this.clientPersistance = clientPersistance;
    }    
    
    @Override
    public Client get(Long id) {
        if(id < 1) {
            throw new InvalidIdentifierException("The specified id must be greater or equals than 1.");
        }

        Client client = clientPersistance.get(id);

        if(client == null) {
            throw new ItemNotFoundException("The specified client was not found.");
        }

        return client;
    }
}
