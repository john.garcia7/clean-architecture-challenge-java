package com.hexagonal.domain.services.client;

import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.exceptions.DuplicatedItemException;
import com.hexagonal.domain.exceptions.ItemNotFoundException;
import com.hexagonal.domain.ports.input.client.IUpdateClientService;
import com.hexagonal.domain.ports.output.IClientPersistance;

public class UpdateClientService implements IUpdateClientService {
    private final IClientPersistance clientPersistance;
    
    public UpdateClientService(IClientPersistance clientPersistance) {
        this.clientPersistance = clientPersistance;
    }

    @Override
    public Client update(Client client) {
        Client existingClient = clientPersistance.get(client.getId());

        if(existingClient == null) {
            throw new ItemNotFoundException("The specified client was not found.");
        }

        existingClient = clientPersistance.getByIdentification(client.getIdType().getIdTypeValue(), client.getIdNumber());
        boolean invalidClient = existingClient != null && !existingClient.getId().equals(client.getId());

        if(invalidClient) {
            throw new DuplicatedItemException("A client with the same identification type and identification number already exists.");
        }

        return clientPersistance.update(client);
    }
}
