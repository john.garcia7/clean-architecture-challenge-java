package com.hexagonal.domain.services.client;

import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.exceptions.MandatoryPageInfoException;
import com.hexagonal.domain.pojos.GenericResultsPage;
import com.hexagonal.domain.pojos.SearchInfo;
import com.hexagonal.domain.ports.input.client.IClientSearchService;
import com.hexagonal.domain.ports.output.IClientPersistance;

public class ClientSearchService implements IClientSearchService {
    private final IClientPersistance clientPersistance;
    
    public ClientSearchService(IClientPersistance clientPersistance) {
        this.clientPersistance = clientPersistance;
    }    
    
    @Override
    public GenericResultsPage<Client> search(SearchInfo searchInfo){
        if(searchInfo.getPageInfo() == null) {
            throw new MandatoryPageInfoException("Missing page info.");
        }

        return clientPersistance.search(searchInfo);
    } 
}
