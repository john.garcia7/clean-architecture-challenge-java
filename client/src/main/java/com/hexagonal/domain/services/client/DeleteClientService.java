package com.hexagonal.domain.services.client;

import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.exceptions.ItemNotFoundException;
import com.hexagonal.domain.ports.input.client.IDeleteClientService;
import com.hexagonal.domain.ports.output.IClientPersistance;

public class DeleteClientService implements IDeleteClientService {
    private final IClientPersistance clientPersistance;
    
    public DeleteClientService(IClientPersistance clientPersistance) {
        this.clientPersistance = clientPersistance;
    }    
    
    @Override
    public void delete(Long id) throws ItemNotFoundException {
        Client client = clientPersistance.get(id);
        
        if(client == null) {
            throw new ItemNotFoundException("The specified client was not found.");
        }

        clientPersistance.delete(id);
    }
}
