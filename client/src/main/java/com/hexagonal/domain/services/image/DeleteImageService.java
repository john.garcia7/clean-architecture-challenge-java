package com.hexagonal.domain.services.image;

import com.hexagonal.domain.exceptions.ItemNotFoundException;
import com.hexagonal.domain.ports.input.image.IDeleteImageService;
import com.hexagonal.domain.ports.output.IImagePersistance;

public class DeleteImageService implements IDeleteImageService {
    private final IImagePersistance imagePersistance;

    public DeleteImageService(IImagePersistance imagePersistance) {
        this.imagePersistance = imagePersistance;
    }

    public void delete(Long personId) {
        if (imagePersistance.get(personId) == null) {
            throw new ItemNotFoundException("An image for the client doesn't exist.");
        }

        imagePersistance.delete(personId);
    }
}
