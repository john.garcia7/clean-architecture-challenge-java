package com.hexagonal.domain.services.image;

import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.exceptions.ItemNotFoundException;
import com.hexagonal.domain.ports.input.image.IUpdateImageService;
import com.hexagonal.domain.ports.output.IImagePersistance;

public class UpdateImageService implements IUpdateImageService {
    private final IImagePersistance imagePersistance;

    public UpdateImageService(IImagePersistance imagePersistance) {
        this.imagePersistance = imagePersistance;
    }

    public void update(Image image) {
        if (imagePersistance.get(image.getPersonId()) == null) {
            throw new ItemNotFoundException("An image for the client doesn't exist.");
        }

        imagePersistance.update(image);
    }
}
