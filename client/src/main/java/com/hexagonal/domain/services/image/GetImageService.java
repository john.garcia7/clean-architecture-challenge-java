package com.hexagonal.domain.services.image;

import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.exceptions.InvalidIdentifierException;
import com.hexagonal.domain.ports.input.image.IGetImageService;
import com.hexagonal.domain.ports.output.IImagePersistance;

public class GetImageService implements IGetImageService {
    private final IImagePersistance imagePersistance;

    public GetImageService(IImagePersistance imagePersistance) {
        this.imagePersistance = imagePersistance;
    }

    public Image get(Long personId) {
        if (personId < 1) {
            throw new InvalidIdentifierException("The specified id must be greater or equals than 1.");
        }

        return imagePersistance.get(personId);
    }
}
