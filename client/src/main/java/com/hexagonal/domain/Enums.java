package com.hexagonal.domain;

public class Enums {
    public enum IdentificationType {
        DNI(0),
        CC(1),
        NIT(2);        

        private int idType;

        IdentificationType(int idType) {
            this.idType = idType;
        }

        public int getIdTypeValue() {
            return idType;
        }
    }
}
