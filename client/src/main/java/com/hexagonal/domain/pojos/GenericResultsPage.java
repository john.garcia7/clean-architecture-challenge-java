package com.hexagonal.domain.pojos;

import java.util.ArrayList;
import java.util.List;

public class GenericResultsPage<T> {

    private PageInfo pageInfo;
    private Long totalResults;
    private int totalPages;
    private List<T> results;    
    
    public GenericResultsPage() {
        results = new ArrayList<>();
    }

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public Long getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Long totalResults) {
        this.totalResults = totalResults;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<T> getResults() {
        return results;
    }

    public void setResults(List<T> results) {
        this.results = results;
    }
}
