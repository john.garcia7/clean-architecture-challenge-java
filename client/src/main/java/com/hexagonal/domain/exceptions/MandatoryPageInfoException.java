package com.hexagonal.domain.exceptions;

public class MandatoryPageInfoException extends RuntimeException {
    public MandatoryPageInfoException(String errorMessage){
        super(errorMessage);
    }
}
