package com.hexagonal.domain.exceptions;

public class InvalidIdentifierException extends RuntimeException {
    public InvalidIdentifierException(String errorMessage){
        super(errorMessage);
    }    
}
