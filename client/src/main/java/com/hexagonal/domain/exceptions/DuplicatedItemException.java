package com.hexagonal.domain.exceptions;

public class DuplicatedItemException extends RuntimeException {
    public DuplicatedItemException(String errorMessage){
        super(errorMessage);
    }    
}
