package com.hexagonal.domain.ports.output;

import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.pojos.GenericResultsPage;
import com.hexagonal.domain.pojos.SearchInfo;

public interface IClientPersistance {
    Client get(Long id);
    Client getByIdentification(Integer identificationType, String identificationNumber);
    GenericResultsPage<Client> search(SearchInfo searchInfo);
    Client add(Client client);
    Client update(Client client);
    void delete(Long id);
}
