package com.hexagonal.domain.ports.input.image;

import com.hexagonal.domain.entites.Image;

public interface IAddImageService {
    void add(Image image);
}
