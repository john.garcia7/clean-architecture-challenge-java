package com.hexagonal.domain.ports.input.image;

import com.hexagonal.domain.entites.Image;

public interface IGetImageService {
    Image get(Long personId);
}
