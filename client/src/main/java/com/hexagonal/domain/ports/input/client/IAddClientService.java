package com.hexagonal.domain.ports.input.client;

import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.exceptions.DuplicatedItemException;

public interface IAddClientService {

    Client add(Client client) throws DuplicatedItemException;
    
}
