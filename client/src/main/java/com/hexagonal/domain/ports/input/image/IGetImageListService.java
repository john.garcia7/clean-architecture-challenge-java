package com.hexagonal.domain.ports.input.image;

import java.util.List;
import java.util.Map;

import com.hexagonal.domain.entites.Image;

public interface IGetImageListService {
    Map<Long, Image> getByClientIdList(List<Long> personIdList);
}
