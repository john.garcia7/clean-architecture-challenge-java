package com.hexagonal.domain.ports.output;

import java.util.List;
import java.util.Map;

import com.hexagonal.domain.entites.Image;

public interface IImagePersistance {
    Image get(Long personId);
    Map<Long, Image> getByClientIdList(List<Long> personIdList);
    void add(Image image);
    void update(Image image);
    void delete(Long personId);
}
