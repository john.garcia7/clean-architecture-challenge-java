package com.hexagonal.domain.ports.input.client;

import com.hexagonal.domain.entites.Client;

public interface IUpdateClientService {

    Client update(Client client);
    
}
