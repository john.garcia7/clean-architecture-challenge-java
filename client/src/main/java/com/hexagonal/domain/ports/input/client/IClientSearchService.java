package com.hexagonal.domain.ports.input.client;

import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.pojos.GenericResultsPage;
import com.hexagonal.domain.pojos.SearchInfo;

public interface IClientSearchService {

    GenericResultsPage<Client> search(SearchInfo searchInfo);
    
}
