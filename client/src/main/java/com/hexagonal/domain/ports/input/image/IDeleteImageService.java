package com.hexagonal.domain.ports.input.image;

public interface IDeleteImageService {
    void delete(Long personId);
}
