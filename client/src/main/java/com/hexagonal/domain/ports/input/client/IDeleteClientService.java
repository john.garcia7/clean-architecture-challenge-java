package com.hexagonal.domain.ports.input.client;

import com.hexagonal.domain.exceptions.ItemNotFoundException;

public interface IDeleteClientService {

    void delete(Long id) throws ItemNotFoundException;
    
}
