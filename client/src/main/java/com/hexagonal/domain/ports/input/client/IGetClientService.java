package com.hexagonal.domain.ports.input.client;

import com.hexagonal.domain.entites.Client;

public interface IGetClientService {

    Client get(Long id);
    
}
