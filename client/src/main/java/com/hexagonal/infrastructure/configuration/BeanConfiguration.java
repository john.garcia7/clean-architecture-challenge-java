package com.hexagonal.infrastructure.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hexagonal.domain.ports.input.client.IAddClientService;
import com.hexagonal.domain.ports.input.client.IClientSearchService;
import com.hexagonal.domain.ports.input.client.IDeleteClientService;
import com.hexagonal.domain.ports.input.client.IGetClientService;
import com.hexagonal.domain.ports.input.client.IUpdateClientService;
import com.hexagonal.domain.ports.input.image.IAddImageService;
import com.hexagonal.domain.ports.input.image.IDeleteImageService;
import com.hexagonal.domain.ports.input.image.IGetImageListService;
import com.hexagonal.domain.ports.input.image.IGetImageService;
import com.hexagonal.domain.ports.input.image.IUpdateImageService;
import com.hexagonal.domain.ports.output.IClientPersistance;
import com.hexagonal.domain.ports.output.IImagePersistance;
import com.hexagonal.domain.services.client.AddClientService;
import com.hexagonal.domain.services.client.ClientSearchService;
import com.hexagonal.domain.services.client.DeleteClientService;
import com.hexagonal.domain.services.client.GetClientService;
import com.hexagonal.domain.services.client.UpdateClientService;
import com.hexagonal.domain.services.image.AddImageService;
import com.hexagonal.domain.services.image.DeleteImageService;
import com.hexagonal.domain.services.image.GetImageListService;
import com.hexagonal.domain.services.image.GetImageService;
import com.hexagonal.domain.services.image.UpdateImageService;
import com.hexagonal.infrastructure.adapters.output.jpa.adapter.ClientAdapter;
import com.hexagonal.infrastructure.adapters.output.jpa.mappers.ClientEntityMapper;
import com.hexagonal.infrastructure.adapters.output.jpa.mappers.ClientEntityPageMapper;
import com.hexagonal.infrastructure.adapters.output.jpa.mappers.IClientEntityMapper;
import com.hexagonal.infrastructure.adapters.output.jpa.mappers.IClientEntityPageMapper;
import com.hexagonal.infrastructure.adapters.output.jpa.repository.ClientRepository;
import com.hexagonal.infrastructure.adapters.output.jpa.specifications.ClientListSpecificationBuilder;
import com.hexagonal.infrastructure.adapters.output.jpa.specifications.IClientListSpecificationBuilder;
import com.hexagonal.infrastructure.adapters.output.mongodb.adapter.ImageMongoDBAdapter;
import com.hexagonal.infrastructure.adapters.output.mongodb.mappers.IImageEntityMapper;
import com.hexagonal.infrastructure.adapters.output.mongodb.mappers.ImageEntityMapper;
import com.hexagonal.infrastructure.adapters.output.mongodb.repository.IImageRepository;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {

    private final ClientRepository clientRepository;
    private final IImageRepository imageRepository;

    @Bean
    public IClientListSpecificationBuilder clientSpecificationBuilder() {
        return new ClientListSpecificationBuilder();
    }

    @Bean
    public IClientEntityMapper clientEntityMapper() {
        return new ClientEntityMapper();
    }

    @Bean
    public IClientEntityPageMapper clientEntityPageMapper() {
        return new ClientEntityPageMapper(clientEntityMapper());
    }

    @Bean
    public IClientPersistance clientPersistance() {
        return new ClientAdapter(
            clientRepository, 
            clientEntityMapper(), 
            clientSpecificationBuilder(), 
            clientEntityPageMapper());
    }


    @Bean
    public IImageEntityMapper imageEntityMapper() {
        return new ImageEntityMapper();
    }

    @Bean
    public IImagePersistance imagePersistance() {
        return new ImageMongoDBAdapter(
            imageRepository, 
            imageEntityMapper());
    }


    @Bean
    public IAddClientService addClientService() {
        return new AddClientService(clientPersistance());
    }

    @Bean
    public IClientSearchService clientSearchService() {
        return new ClientSearchService(clientPersistance());
    }

    @Bean
    public IDeleteClientService deleteClientService() {
        return new DeleteClientService(clientPersistance());
    }

    @Bean
    public IGetClientService getClientService() {
        return new GetClientService(clientPersistance());
    }

    @Bean
    public IUpdateClientService updateClientService() {
        return new UpdateClientService(clientPersistance());
    }

    
    @Bean
    public IAddImageService addImageService() {
        return new AddImageService(imagePersistance());
    }

    @Bean
    public IDeleteImageService deleteImageService() {
        return new DeleteImageService(imagePersistance());
    }

    @Bean
    public IGetImageListService getImageListService() {
        return new GetImageListService(imagePersistance());
    }

    @Bean
    public IGetImageService getImageService() {
        return new GetImageService(imagePersistance());
    }

    @Bean
    public IUpdateImageService updateImageService() {
        return new UpdateImageService(imagePersistance());
    }
}
