package com.hexagonal.infrastructure.adapters.output.jpa.specifications;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SearchCriteria {
    private String key;
    private Object value;
    private SearchOperation operation;

    public enum SearchOperation {
        EQUAL,
        GREATER_THAN_EQUAL
    }
}
