package com.hexagonal.infrastructure.adapters.output.jpa.mappers;

import com.hexagonal.domain.entites.Client;
import com.hexagonal.infrastructure.adapters.output.jpa.entities.ClientEntity;

public interface IClientEntityMapper {
    
    ClientEntity toClientEntity(Client client);

    Client toClient(ClientEntity clientEntity);

}
