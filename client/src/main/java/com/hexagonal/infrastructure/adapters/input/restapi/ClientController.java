package com.hexagonal.infrastructure.adapters.input.restapi;

import lombok.RequiredArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.hexagonal.application.dto.ClientDto;
import com.hexagonal.application.dto.SearchDto;
import com.hexagonal.application.interfaces.handlers.*;
import com.hexagonal.domain.pojos.GenericResultsPage;

@RestController
@RequestMapping("/client")
@RequiredArgsConstructor
public class ClientController {
    private final IAddClientHandler addClientHandler;
    private final IClientSearchHandler clientSearchHandler;
    private final IDeleteClientHandler deleteClientHandler;
    private final IGetClientHandler getClientHandler;
    private final IUpdateClientHandler updateClientHandler;

    @GetMapping("/{id}")
    public ResponseEntity<ClientDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(getClientHandler.get(id)); 
    }

    @PostMapping("/list")
    public ResponseEntity<GenericResultsPage<ClientDto>> search(@RequestBody SearchDto searchInfo) {
        return ResponseEntity.ok(clientSearchHandler.search(searchInfo));
    }

    @PostMapping()
    public ResponseEntity<ClientDto> create(@RequestBody ClientDto clientDto) {
        return new ResponseEntity<>(addClientHandler.add(clientDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ClientDto> update(@PathVariable Long id, @RequestBody ClientDto clientDto) {
        return ResponseEntity.ok(updateClientHandler.update(id, clientDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        deleteClientHandler.delete(id);

        return ResponseEntity.noContent().build();
    }
}
