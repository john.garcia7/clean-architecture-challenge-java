package com.hexagonal.infrastructure.adapters.output.jpa.specifications;

import org.springframework.data.jpa.domain.Specification;

import com.hexagonal.infrastructure.adapters.output.jpa.entities.ClientEntity;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class ClientSpecification implements Specification<ClientEntity> {
    private List<SearchCriteria> criteriaList;

    public ClientSpecification() {
        criteriaList = new ArrayList<>();
    }

    public void addSearchCriteria(SearchCriteria searchCriteria) {
        criteriaList.add(searchCriteria);
    }

    @Override
    public Predicate toPredicate(Root<ClientEntity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicateList = new ArrayList<>();

        for (SearchCriteria searchCriteria : criteriaList) {
            if(searchCriteria.getOperation() == SearchCriteria.SearchOperation.EQUAL) {
                predicateList.add(criteriaBuilder.equal(root.get(searchCriteria.getKey()), searchCriteria.getValue()));
            } else if (searchCriteria.getOperation() == SearchCriteria.SearchOperation.GREATER_THAN_EQUAL) {
                predicateList.add(criteriaBuilder.greaterThanOrEqualTo(root.get(searchCriteria.getKey()), searchCriteria.getValue().toString()));
            }
        }

        return criteriaBuilder.and(predicateList.toArray(new Predicate[0]));
    }
}
