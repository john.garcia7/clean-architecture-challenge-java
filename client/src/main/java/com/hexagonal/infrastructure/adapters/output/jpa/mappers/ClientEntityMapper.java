package com.hexagonal.infrastructure.adapters.output.jpa.mappers;

import com.hexagonal.domain.Enums;
import com.hexagonal.domain.entites.Client;
import com.hexagonal.infrastructure.adapters.output.jpa.entities.ClientEntity;

public class ClientEntityMapper implements IClientEntityMapper {
    
    public ClientEntity toClientEntity(Client client) {
        if (client == null) {
            return null;
        }

        ClientEntity clientEntity = new ClientEntity();
        
        clientEntity.setId(client.getId());
        clientEntity.setFirstName(client.getFirstName());
        clientEntity.setLastName(client.getLastName());
        clientEntity.setIdNumber(client.getIdNumber());
        clientEntity.setIdType(client.getIdType().getIdTypeValue());
        clientEntity.setAge(client.getAge());

        return clientEntity;
    }

    public Client toClient(ClientEntity clientEntity) {
        if (clientEntity == null) {
            return null;
        }

        Client client = new Client();
        
        client.setId(clientEntity.getId());
        client.setFirstName(clientEntity.getFirstName());
        client.setLastName(clientEntity.getLastName());
        client.setIdNumber(clientEntity.getIdNumber());
        client.setIdType(Enums.IdentificationType.values()[clientEntity.getIdType()]);
        client.setAge(clientEntity.getAge());

        return client;
    }

}
