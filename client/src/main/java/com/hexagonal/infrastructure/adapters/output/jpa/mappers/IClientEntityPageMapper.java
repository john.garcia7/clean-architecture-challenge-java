package com.hexagonal.infrastructure.adapters.output.jpa.mappers;

import org.springframework.data.domain.Page;

import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.pojos.GenericResultsPage;
import com.hexagonal.domain.pojos.PageInfo;
import com.hexagonal.infrastructure.adapters.output.jpa.entities.ClientEntity;

public interface IClientEntityPageMapper {
    
    GenericResultsPage<Client> mapPage(PageInfo sourcePageInfo, Page<ClientEntity> sourcePage);

}
