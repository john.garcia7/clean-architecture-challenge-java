package com.hexagonal.infrastructure.adapters.output.mongodb.mappers;

import com.hexagonal.domain.entites.Image;
import com.hexagonal.infrastructure.adapters.output.mongodb.entity.ImageEntity;

public class ImageEntityMapper implements IImageEntityMapper {
    
    public ImageEntity toImageEntity(Image image) {
        if (image == null) {
            return null;
        }

        ImageEntity imageEntity = new ImageEntity();

        imageEntity.setPersonId(image.getPersonId());
        imageEntity.setImageData(image.getImageData());

        return imageEntity;
    }

    public ImageEntity toImageEntity(Image image, String id) {
        ImageEntity imageEntity = toImageEntity(image);

        if (imageEntity == null) {
            return null;
        }

        imageEntity.setId(id);

        return imageEntity;
    }

    public Image toImage(ImageEntity imageEntity) {
        if (imageEntity == null) {
            return null;
        }

        Image image = new Image();

        image.setPersonId(imageEntity.getPersonId());
        image.setImageData(imageEntity.getImageData());

        return image;
    }

}
