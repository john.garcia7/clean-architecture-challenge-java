package com.hexagonal.infrastructure.adapters.output.mongodb.mappers;

import com.hexagonal.domain.entites.Image;
import com.hexagonal.infrastructure.adapters.output.mongodb.entity.ImageEntity;

public interface IImageEntityMapper {
    ImageEntity toImageEntity(Image image);
    ImageEntity toImageEntity(Image image, String id);
    Image toImage(ImageEntity imageEntity);    
}
