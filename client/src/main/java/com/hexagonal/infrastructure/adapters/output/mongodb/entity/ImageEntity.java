package com.hexagonal.infrastructure.adapters.output.mongodb.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.Data;

@Document(collection = "images")
@Data
public class ImageEntity {
    @Id
    private String id;

    @Field(name = "PersonId")
    private Long personId;

    @Field(name = "ImageData")
    private String imageData;
}
