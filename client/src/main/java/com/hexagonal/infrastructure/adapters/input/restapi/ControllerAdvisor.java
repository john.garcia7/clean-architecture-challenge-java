package com.hexagonal.infrastructure.adapters.input.restapi;

import java.util.Collections;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.hexagonal.domain.exceptions.DuplicatedItemException;
import com.hexagonal.domain.exceptions.InvalidIdentifierException;
import com.hexagonal.domain.exceptions.ItemNotFoundException;
import com.hexagonal.domain.exceptions.MandatoryPageInfoException;

@ControllerAdvice
public class ControllerAdvisor {
    
    private static final String MESSAGE = "Message";

    @ExceptionHandler(ItemNotFoundException.class)
    public ResponseEntity<Map<String, String>> handleItemNotFoundException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Collections.singletonMap(MESSAGE, "The item was not found."));
    }

    @ExceptionHandler(DuplicatedItemException.class)
    public ResponseEntity<Map<String, String>> handleDuplicatedItemException() {
        return ResponseEntity.status(HttpStatus.ALREADY_REPORTED).body(Collections.singletonMap(MESSAGE, "The item can't be created or updated because is in conflict with another existent element."));
    }

    @ExceptionHandler(InvalidIdentifierException.class)
    public ResponseEntity<Map<String, String>> handleInvalidIdentifierException() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, "The id of the element must be greater than zero."));
    }

    @ExceptionHandler(MandatoryPageInfoException.class)
    public ResponseEntity<Map<String, String>> handleMandatoryPageInfoException() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, "The page information is mandatory."));
    }
}
