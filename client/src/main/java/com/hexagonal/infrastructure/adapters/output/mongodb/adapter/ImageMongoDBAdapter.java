package com.hexagonal.infrastructure.adapters.output.mongodb.adapter;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.ports.output.IImagePersistance;
import com.hexagonal.infrastructure.adapters.output.mongodb.entity.ImageEntity;
import com.hexagonal.infrastructure.adapters.output.mongodb.mappers.IImageEntityMapper;
import com.hexagonal.infrastructure.adapters.output.mongodb.repository.IImageRepository;

public class ImageMongoDBAdapter implements IImagePersistance {

    private IImageRepository imageRepository;
    private IImageEntityMapper imageEntityMapper;

    public ImageMongoDBAdapter(
        IImageRepository imageRepository,
        IImageEntityMapper imageEntityMapper) {
        this.imageRepository = imageRepository;
        this.imageEntityMapper = imageEntityMapper;
    }

    @Override
    public Image get(Long personId) {
        return imageEntityMapper.toImage(imageRepository.findByPersonId(personId));
    }

    @Override
    public Map<Long, Image> getByClientIdList(List<Long> personIdList) {                
        List<ImageEntity> imageEntityList = imageRepository.findByPersonIdIn(personIdList);
        List<Image> imageList = imageEntityList.stream().map(imageEntity -> imageEntityMapper.toImage(imageEntity)).collect(Collectors.toList());
        
        return imageList.stream().collect(Collectors.toMap(Image::getPersonId, Function.identity()));
    }

    @Override
    public void add(Image image) {
        imageRepository.insert(imageEntityMapper.toImageEntity(image));
    }

    @Override
    public void update(Image image) {
        ImageEntity currentImage = imageRepository.findByPersonId(image.getPersonId());

        imageRepository.save(imageEntityMapper.toImageEntity(image, currentImage.getId()));
    }

    @Override
    public void delete(Long personId) {
        imageRepository.deleteByPersonId(personId);
    }
    
}
