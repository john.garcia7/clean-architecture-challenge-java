package com.hexagonal.infrastructure.adapters.output.jpa.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "clients")
@Getter
@Setter
public class ClientEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "firstname")
    private String firstName;
    @Column(name = "lastname")
    private String lastName;
    @Column(name = "idtype")
    private Integer idType;
    @Column(name = "idnumber")
    private String idNumber;
    private Long age;
    @Column(name = "cityofbirth")
    private String cityOfBirth;
}
