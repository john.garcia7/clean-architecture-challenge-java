package com.hexagonal.infrastructure.adapters.output.jpa.specifications;

import com.hexagonal.domain.pojos.SearchFilter;

public interface IClientListSpecificationBuilder {
    
    ClientSpecification build(SearchFilter searchFilter);

}
