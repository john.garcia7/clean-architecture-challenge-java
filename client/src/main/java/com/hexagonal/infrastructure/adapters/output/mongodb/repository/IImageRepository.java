package com.hexagonal.infrastructure.adapters.output.mongodb.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.hexagonal.infrastructure.adapters.output.mongodb.entity.ImageEntity;

public interface IImageRepository extends MongoRepository<ImageEntity, Long> {
    
    ImageEntity findByPersonId(Long personId);

    List<ImageEntity> findByPersonIdIn(List<Long> personIdList);

    void deleteByPersonId(Long personId);

}
