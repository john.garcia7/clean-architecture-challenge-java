package com.hexagonal.infrastructure.adapters.output.jpa.specifications;

import com.hexagonal.domain.pojos.SearchFilter;

public class ClientListSpecificationBuilder implements IClientListSpecificationBuilder {
    
    public ClientSpecification build(SearchFilter searchFilter) {
        ClientSpecification clientSpecification = new ClientSpecification();

        if (searchFilter == null) {
            return clientSpecification;
        }

        if(searchFilter.getIdType() != null && searchFilter.getIdType() >= 0) {
            clientSpecification.addSearchCriteria(new SearchCriteria("idType", searchFilter.getIdType(), SearchCriteria.SearchOperation.EQUAL));
        }

        if(searchFilter.getIdNumber() != null && !searchFilter.getIdNumber().isBlank()) {
            clientSpecification.addSearchCriteria(new SearchCriteria("idNumber", searchFilter.getIdNumber(), SearchCriteria.SearchOperation.EQUAL));
        }

        if(searchFilter.getAge() != null && searchFilter.getAge() > 0) {
            clientSpecification.addSearchCriteria(new SearchCriteria("age", searchFilter.getAge(), SearchCriteria.SearchOperation.GREATER_THAN_EQUAL));
        }

        return clientSpecification;
    }

}
