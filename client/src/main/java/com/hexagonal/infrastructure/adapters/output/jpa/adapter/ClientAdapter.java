package com.hexagonal.infrastructure.adapters.output.jpa.adapter;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.pojos.GenericResultsPage;
import com.hexagonal.domain.pojos.SearchFilter;
import com.hexagonal.domain.pojos.SearchInfo;
import com.hexagonal.domain.ports.output.IClientPersistance;
import com.hexagonal.infrastructure.adapters.output.jpa.entities.ClientEntity;
import com.hexagonal.infrastructure.adapters.output.jpa.mappers.IClientEntityMapper;
import com.hexagonal.infrastructure.adapters.output.jpa.mappers.IClientEntityPageMapper;
import com.hexagonal.infrastructure.adapters.output.jpa.repository.ClientRepository;
import com.hexagonal.infrastructure.adapters.output.jpa.specifications.ClientSpecification;
import com.hexagonal.infrastructure.adapters.output.jpa.specifications.IClientListSpecificationBuilder;

import java.util.Optional;

public class ClientAdapter implements IClientPersistance {

    private final ClientRepository clientRepository;
    private final IClientEntityMapper clientEntityMapper;
    private final IClientListSpecificationBuilder clientSpecificationBuilder;
    private final IClientEntityPageMapper clientEntityPageMapper;

    public ClientAdapter(
        ClientRepository clientRepository,
        IClientEntityMapper clientEntityMapper,
        IClientListSpecificationBuilder clientSpecificationBuilder,
        IClientEntityPageMapper clientEntityPageMapper) {
        this.clientRepository = clientRepository;
        this.clientEntityMapper = clientEntityMapper;
        this.clientSpecificationBuilder = clientSpecificationBuilder;
        this.clientEntityPageMapper = clientEntityPageMapper;
    }

    @Override
    public Client get(Long id) {
        Client client = null;

        Optional<ClientEntity> clientEntity = clientRepository.findById(id);

        if (clientEntity.isPresent()) {
            client = clientEntityMapper.toClient(clientEntity.get());
        }

        return client;
    }

    @Override
    public Client getByIdentification(Integer identificationType, String identificationNumber) {
        Client client = null;

        SearchFilter searchFilter = new SearchFilter();
        searchFilter.setIdType(identificationType);
        searchFilter.setIdNumber(identificationNumber);

        Optional<ClientEntity> clientEntity = clientRepository.findOne(clientSpecificationBuilder.build(searchFilter));

        if(clientEntity.isPresent()) {
            client = clientEntityMapper.toClient(clientEntity.get());
        }

        return client;
    }

    @Override
    public GenericResultsPage<Client> search(SearchInfo searchInfo) {
        Pageable pageable = PageRequest.of(searchInfo.getPageInfo().getPage() - 1, searchInfo.getPageInfo().getPageSize());

        ClientSpecification clientSpecification = clientSpecificationBuilder.build(searchInfo.getSearchFilter());

        Page<ClientEntity> clients = clientRepository.findAll(clientSpecification, pageable);

        return clientEntityPageMapper.mapPage(searchInfo.getPageInfo(), clients);
    }

    @Override
    public Client add(Client client) {
        ClientEntity clientToSave = clientEntityMapper.toClientEntity(client);

        ClientEntity savedClient = clientRepository.save(clientToSave);

        return clientEntityMapper.toClient(savedClient);
    }

    @Override
    public Client update(Client client) {
        ClientEntity clientEntity = clientRepository.save(clientEntityMapper.toClientEntity(client));

        return clientEntityMapper.toClient(clientEntity);
    }

    @Override
    public void delete(Long id) {
        clientRepository.deleteById(id);
    }
}
