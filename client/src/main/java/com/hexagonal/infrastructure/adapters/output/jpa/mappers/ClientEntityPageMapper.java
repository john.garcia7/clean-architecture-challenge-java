package com.hexagonal.infrastructure.adapters.output.jpa.mappers;

import org.springframework.data.domain.Page;

import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.pojos.GenericResultsPage;
import com.hexagonal.domain.pojos.PageInfo;
import com.hexagonal.infrastructure.adapters.output.jpa.entities.ClientEntity;

import java.util.stream.Collectors;

public class ClientEntityPageMapper implements IClientEntityPageMapper {
    
    IClientEntityMapper clientEntityMapper;

    public ClientEntityPageMapper(IClientEntityMapper clientEntityMapper) {
        this.clientEntityMapper = clientEntityMapper;
    }

    public GenericResultsPage<Client> mapPage(PageInfo sourcePageInfo, Page<ClientEntity> sourcePage) {
        
        GenericResultsPage<Client> page = new GenericResultsPage<>();

        page.setPageInfo(sourcePageInfo);
        page.setTotalPages(sourcePage.getTotalPages());
        page.setTotalResults(sourcePage.getTotalElements());
        page.setResults(sourcePage.getContent().stream().map(client -> clientEntityMapper.toClient(client)).collect(Collectors.toList()));

        return page;
    }

}
