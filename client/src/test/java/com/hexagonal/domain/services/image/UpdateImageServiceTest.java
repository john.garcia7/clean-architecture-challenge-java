package com.hexagonal.domain.services.image;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.exceptions.ItemNotFoundException;
import com.hexagonal.domain.ports.input.image.IUpdateImageService;
import com.hexagonal.domain.ports.output.IImagePersistance;

public class UpdateImageServiceTest {
    private IUpdateImageService updateImageService;
    private IImagePersistance imagePersistanceMock;

    public UpdateImageServiceTest() {
        imagePersistanceMock = mock(IImagePersistance.class);
        updateImageService = new UpdateImageService(imagePersistanceMock);
    }

    @Test
    void updateTest() {
        // Arrange
        Image updatedImage = new Image();
        updatedImage.setPersonId(1L);
        updatedImage.setImageData("base64/updatedImageData");

        Image currentImage = new Image();
        currentImage.setPersonId(1L);
        currentImage.setImageData("base64/imageData");

        when(imagePersistanceMock.get(updatedImage.getPersonId())).thenReturn(currentImage);

        // Act
        updateImageService.update(updatedImage);

        // Assert
        verify(imagePersistanceMock, times(1)).update(updatedImage);
    }

    @Test
    void imageNotFoundException() {
        // Arrange
        Image image = new Image();
        image.setPersonId(1L);
        image.setImageData("base64/imageData");

        when(imagePersistanceMock.get(image.getPersonId())).thenReturn(null);

        // Act and Assert
        assertThrows(ItemNotFoundException.class, () -> updateImageService.update(image));
        verify(imagePersistanceMock, times(0)).update(image);
    }
}
