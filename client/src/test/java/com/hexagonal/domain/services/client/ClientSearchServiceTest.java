package com.hexagonal.domain.services.client;

import java.util.ArrayList;
import org.junit.jupiter.api.Test;

import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.exceptions.MandatoryPageInfoException;
import com.hexagonal.domain.pojos.GenericResultsPage;
import com.hexagonal.domain.pojos.PageInfo;
import com.hexagonal.domain.pojos.SearchInfo;
import com.hexagonal.domain.ports.input.client.IClientSearchService;
import com.hexagonal.domain.ports.output.IClientPersistance;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ClientSearchServiceTest {
    
    private IClientSearchService clientSearchService;
    private IClientPersistance clientPersistanceMock;
    
    public ClientSearchServiceTest() {
        clientPersistanceMock = mock(IClientPersistance.class);
        clientSearchService = new ClientSearchService(clientPersistanceMock);
    }

    // Test Methods

    @Test
    public void getAllTest() {
        // Arrange
        PageInfo pageInfo = new PageInfo();
        pageInfo.setPage(1);
        pageInfo.setPageSize(10);

        SearchInfo searchInfo = new SearchInfo();
        searchInfo.setPageInfo(pageInfo);
        
        GenericResultsPage<Client> expectedClientsPage = new GenericResultsPage<>();
        expectedClientsPage.setPageInfo(pageInfo);
        expectedClientsPage.setResults(new ArrayList<Client>());
        expectedClientsPage.setTotalPages(1);
        expectedClientsPage.setTotalResults(2L);
        
        when(clientPersistanceMock.search(searchInfo)).thenReturn(expectedClientsPage);
        
        // Act
        GenericResultsPage<Client> clientsPage = clientSearchService.search(searchInfo);
        
        // Assert
        assertEquals(expectedClientsPage, clientsPage);
    }
    
    @Test
    public void mandatoryPageInfoTest() {
        // Arrange
        SearchInfo searchInfo = new SearchInfo(); 
        searchInfo.setPageInfo(null);
        
        // Act and Assert
        assertThrows(MandatoryPageInfoException.class, () -> {
            clientSearchService.search(searchInfo);
        });
        
        verify(clientPersistanceMock, times(0)).search(searchInfo);
    }
}
