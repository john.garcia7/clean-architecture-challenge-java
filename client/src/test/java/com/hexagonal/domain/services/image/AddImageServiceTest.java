package com.hexagonal.domain.services.image;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.exceptions.DuplicatedItemException;
import com.hexagonal.domain.ports.input.image.IAddImageService;
import com.hexagonal.domain.ports.output.IImagePersistance;

public class AddImageServiceTest {
    private IAddImageService addImageService;
    private IImagePersistance imagePersistanceMock;

    public AddImageServiceTest() {
        imagePersistanceMock = mock(IImagePersistance.class);
        addImageService = new AddImageService(imagePersistanceMock);
    }

    @Test
    void addTest() {
        // Arrange
        Image image = new Image();
        image.setPersonId(1L);
        image.setImageData("base64/imageData");

        // Act
        addImageService.add(image);

        // Assert
        verify(imagePersistanceMock, times(1)).add(image);
    }

    @Test
    void duplicatedClientImageTest() {
        // Arrange
        Image newImage = new Image();
        newImage.setPersonId(1L);
        newImage.setImageData("base64/imageData");

        Image currentImage = new Image();
        currentImage.setPersonId(1L);
        currentImage.setImageData("Another base64/imageData");

        when(imagePersistanceMock.get(newImage.getPersonId())).thenReturn(currentImage);

        // Act and Assert
        assertThrows(DuplicatedItemException.class, () -> addImageService.add(newImage));
        verify(imagePersistanceMock, times(0)).add(newImage);
    }
}
