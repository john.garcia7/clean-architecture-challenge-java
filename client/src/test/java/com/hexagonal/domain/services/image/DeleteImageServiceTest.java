package com.hexagonal.domain.services.image;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.exceptions.ItemNotFoundException;
import com.hexagonal.domain.ports.input.image.IDeleteImageService;
import com.hexagonal.domain.ports.output.IImagePersistance;

public class DeleteImageServiceTest {
    private IDeleteImageService deleteImageService;
    private IImagePersistance imagePersistanceMock;

    public DeleteImageServiceTest() {
        imagePersistanceMock = mock(IImagePersistance.class);
        deleteImageService = new DeleteImageService(imagePersistanceMock);
    }

    @Test
    void deleteTest() {
        // Arrange
        Long personImageIdToDelete = 1L;

        Image currentImage = new Image();
        currentImage.setPersonId(personImageIdToDelete);
        currentImage.setImageData("base64/imageData");

        when(imagePersistanceMock.get(personImageIdToDelete)).thenReturn(currentImage);

        // Act
        deleteImageService.delete(personImageIdToDelete);

        // Assert
        verify(imagePersistanceMock, times(1)).delete(personImageIdToDelete);
    }

    @Test
    void ImageToDeleteNotExists() {
        // Arrange
        Long personImageIdToDelete = 1L;

        when(imagePersistanceMock.get(personImageIdToDelete)).thenReturn(null);

        // Act and Assert
        assertThrows(ItemNotFoundException.class, () -> deleteImageService.delete(personImageIdToDelete));
        verify(imagePersistanceMock, times(0)).delete(personImageIdToDelete);
    }
}
