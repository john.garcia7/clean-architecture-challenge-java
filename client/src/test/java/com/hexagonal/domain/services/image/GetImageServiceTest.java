package com.hexagonal.domain.services.image;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.exceptions.InvalidIdentifierException;
import com.hexagonal.domain.ports.input.image.IGetImageService;
import com.hexagonal.domain.ports.output.IImagePersistance;

public class GetImageServiceTest {
    private IGetImageService getImageService;
    private IImagePersistance imagePersistanceMock;
    
    public GetImageServiceTest() {
        imagePersistanceMock = mock(IImagePersistance.class);
        getImageService = new GetImageService(imagePersistanceMock);
    }

    @Test
    void getTest() {
        // Arrange
        Long personId = 1L;

        Image expectedImage = new Image();
        expectedImage.setPersonId(personId);
        expectedImage.setImageData("base64/imageData");

        when(imagePersistanceMock.get(personId)).thenReturn(expectedImage);
        
        // Act
        Image image = getImageService.get(personId);

        // Assert
        assertEquals(expectedImage, image);
        verify(imagePersistanceMock, times(1)).get(personId);
    }

    @Test
    void invalidIdentifierTest() {
        // Arrange
        Long personId = -1L;

        // Act and Assert
        assertThrows(InvalidIdentifierException.class, () -> getImageService.get(personId));
    }
}
