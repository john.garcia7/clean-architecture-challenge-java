package com.hexagonal.domain.services.client;

import org.junit.jupiter.api.Test;

import com.hexagonal.domain.Enums;
import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.exceptions.DuplicatedItemException;
import com.hexagonal.domain.ports.input.client.IAddClientService;
import com.hexagonal.domain.ports.output.IClientPersistance;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AddClientServiceTest {
    
    private IAddClientService addClientService;
    private IClientPersistance clientPersistanceMock;
    
    public AddClientServiceTest() {
        clientPersistanceMock = mock(IClientPersistance.class);
        addClientService = new AddClientService(clientPersistanceMock);
    }

    // Test Methods
    
    @Test
    public void addClientTest() {
        // Arrange
        Client client = new Client();
        client.setFirstName("Sharon");
        client.setLastName("Stone");
        client.setAge(35L);
        client.setCityOfBirth("NY");
        client.setIdType(Enums.IdentificationType.CC);
        client.setIdNumber("354564564");
        
        // Act
        addClientService.add(client);
        
        // Assert
        verify(clientPersistanceMock, times(1)).add(client);
    }
    
    @Test
    public void duplicatedClientTest() {
        // Arrange
        Client existingClient = new Client();
        existingClient.setFirstName("Sharon");
        existingClient.setLastName("Stone");
        existingClient.setAge(35L);
        existingClient.setCityOfBirth("NY");
        existingClient.setIdType(Enums.IdentificationType.DNI);
        existingClient.setIdNumber("354564564");

        Client newClient = new Client();
        newClient.setFirstName("Demi");
        newClient.setLastName("Moore");
        newClient.setAge(40L);
        newClient.setCityOfBirth("Washington");
        newClient.setIdType(Enums.IdentificationType.DNI);
        newClient.setIdNumber("354564564");
        
        when(clientPersistanceMock.getByIdentification(newClient.getIdType().getIdTypeValue(), newClient.getIdNumber())).thenReturn(existingClient);
        
        // Act and Assert
        assertThrows(DuplicatedItemException.class, () -> {
            addClientService.add(newClient);
        });
        
        verify(clientPersistanceMock, times(0)).add(newClient);
    }
}
