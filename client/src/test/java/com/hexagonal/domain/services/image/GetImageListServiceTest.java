package com.hexagonal.domain.services.image;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.hexagonal.domain.entites.Image;
import com.hexagonal.domain.ports.input.image.IGetImageListService;
import com.hexagonal.domain.ports.output.IImagePersistance;

public class GetImageListServiceTest {
    private IGetImageListService getImageListService;
    private IImagePersistance imagePersistanceMock;

    public GetImageListServiceTest() {
        imagePersistanceMock = mock(IImagePersistance.class);
        getImageListService = new GetImageListService(imagePersistanceMock);
    }

    @Test
    void testGetByClientIdList() {
        // Arrange
        List<Long> personIdList = new ArrayList<>(List.of(1L, 2L));
        
        Image expectedImage1 = new Image();
        expectedImage1.setPersonId(1L);
        expectedImage1.setImageData("base64/imageData1");

        Image expectedImage2 = new Image();
        expectedImage2.setPersonId(2L);
        expectedImage2.setImageData("base64/imageData2");

        Map<Long, Image> expectedDictMap = Map.ofEntries(
            new AbstractMap.SimpleEntry<Long, Image>(1L, expectedImage1),
            new AbstractMap.SimpleEntry<Long, Image>(2L, expectedImage2));

        when(imagePersistanceMock.getByClientIdList(personIdList)).thenReturn(expectedDictMap);

        // Act
        Map<Long, Image> imagesDictMap = getImageListService.getByClientIdList(personIdList);

        // Assert
        assertEquals(expectedDictMap, imagesDictMap);
    }
}
