package com.hexagonal.domain.services.client;

import org.junit.jupiter.api.Test;

import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.exceptions.ItemNotFoundException;
import com.hexagonal.domain.ports.input.client.IDeleteClientService;
import com.hexagonal.domain.ports.output.IClientPersistance;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class DeleteClientServiceTest {
    
    private IDeleteClientService deleteClientService;
    private IClientPersistance clientPersistanceMock;
    
    public DeleteClientServiceTest() {
        clientPersistanceMock = mock(IClientPersistance.class);
        deleteClientService = new DeleteClientService(clientPersistanceMock);
    }

    // Test Methods

    @Test
    public void deleteTest() {
        // Arrange
        Long idToDelete = 5L;
        Client client = new Client(); 
        client.setId(idToDelete);

        when(clientPersistanceMock.get(idToDelete)).thenReturn(client);
        
        // Act
        deleteClientService.delete(idToDelete);
        
        // Assert
        verify(clientPersistanceMock, times(1)).delete(idToDelete);
    }
    
    @Test
    public void ItemToDeleteNotFoundTest() {
        // Arrange
        Long idToDelete = 5L;
        
        // Act
        assertThrows(ItemNotFoundException.class, () -> {
            deleteClientService.delete(idToDelete);
        });        
        
        // Assert
        verify(clientPersistanceMock, times(1)).get(idToDelete);
        verify(clientPersistanceMock, times(0)).delete(idToDelete);
    }
}
