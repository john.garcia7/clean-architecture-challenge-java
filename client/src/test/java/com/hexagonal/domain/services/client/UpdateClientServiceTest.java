package com.hexagonal.domain.services.client;

import org.junit.jupiter.api.Test;

import com.hexagonal.domain.Enums;
import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.exceptions.DuplicatedItemException;
import com.hexagonal.domain.exceptions.ItemNotFoundException;
import com.hexagonal.domain.ports.input.client.IUpdateClientService;
import com.hexagonal.domain.ports.output.IClientPersistance;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class UpdateClientServiceTest {
    
    private IUpdateClientService updateClientService;
    private IClientPersistance clientPersistanceMock;
    
    public UpdateClientServiceTest() {
        clientPersistanceMock = mock(IClientPersistance.class);
        updateClientService = new UpdateClientService(clientPersistanceMock);
    }

    // Test Methods

    @Test
    public void updateClientTest() {
        // Arrange
        Client existingClient = new Client();
        existingClient.setId(1L);
        existingClient.setFirstName("Sharon");
        existingClient.setLastName("Stone");
        existingClient.setAge(35L);
        existingClient.setCityOfBirth("NY");
        existingClient.setIdType(Enums.IdentificationType.DNI);
        existingClient.setIdNumber("354564564");

        Client updatedClient = new Client();
        updatedClient.setId(1L);
        updatedClient.setFirstName("Demi");
        updatedClient.setLastName("Moore");
        updatedClient.setAge(40L);
        updatedClient.setCityOfBirth("Washington");
        updatedClient.setIdType(Enums.IdentificationType.DNI);
        updatedClient.setIdNumber("354564564");

        when(clientPersistanceMock.get(updatedClient.getId())).thenReturn(updatedClient);
        when(clientPersistanceMock.getByIdentification(updatedClient.getIdType().getIdTypeValue(), updatedClient.getIdNumber())).thenReturn(existingClient);

        // Act
        updateClientService.update(updatedClient);

        // Assert
        verify(clientPersistanceMock, times(1)).update(updatedClient);
    }
    
    @Test
    public void duplicatedClientByIdentificationTest(){
        // Arrange
        Client anOtherExistingClient = new Client();
        anOtherExistingClient.setId(1L);
        anOtherExistingClient.setFirstName("Sharon");
        anOtherExistingClient.setLastName("Stone");
        anOtherExistingClient.setAge(35L);
        anOtherExistingClient.setCityOfBirth("NY");
        anOtherExistingClient.setIdType(Enums.IdentificationType.DNI);
        anOtherExistingClient.setIdNumber("354564564");

        Client updatedClient = new Client();
        updatedClient.setId(2L);
        updatedClient.setFirstName("Demi");
        updatedClient.setLastName("Moore");
        updatedClient.setAge(40L);
        updatedClient.setCityOfBirth("Washington");
        updatedClient.setIdType(Enums.IdentificationType.DNI);
        updatedClient.setIdNumber("354564564");
        
        when(clientPersistanceMock.get(updatedClient.getId())).thenReturn(updatedClient);
        when(clientPersistanceMock.getByIdentification(updatedClient.getIdType().getIdTypeValue(), updatedClient.getIdNumber())).thenReturn(anOtherExistingClient);
        
        // Act and Assert
        assertThrows(DuplicatedItemException.class, () -> {
            updateClientService.update(updatedClient);
        });
        
        verify(clientPersistanceMock, times(0)).update(updatedClient);
    }

    @Test
    public void updateWithNewIdentificationInfoTest(){
        // Arrange
        Client anotherExistingClient = null;

        Client updatedClient = new Client();
        updatedClient.setId(1L);
        updatedClient.setFirstName("Demi");
        updatedClient.setLastName("Moore");
        updatedClient.setAge(40L);
        updatedClient.setCityOfBirth("Washington");
        updatedClient.setIdType(Enums.IdentificationType.DNI);
        updatedClient.setIdNumber("354564564");

        when(clientPersistanceMock.get(updatedClient.getId())).thenReturn(updatedClient);
        when(clientPersistanceMock.getByIdentification(updatedClient.getIdType().getIdTypeValue(), updatedClient.getIdNumber())).thenReturn(anotherExistingClient);

        // Act
        updateClientService.update(updatedClient);

        // Assert
        verify(clientPersistanceMock, times(1)).update(updatedClient);
    }
    
    @Test
    public void clientNotFoundTest(){
        // Arrange
        Client updatedClient = new Client();
        updatedClient.setId(2L);
        updatedClient.setFirstName("Demi");
        updatedClient.setLastName("Moore");
        updatedClient.setAge(40L);
        updatedClient.setCityOfBirth("Washington");
        updatedClient.setIdType(Enums.IdentificationType.DNI);
        updatedClient.setIdNumber("354564564");
        
        Client existingClient = null;
        
        when(clientPersistanceMock.get(updatedClient.getId())).thenReturn(existingClient);
        
        // Act and Assert
        assertThrows(ItemNotFoundException.class, () -> {
            updateClientService.update(updatedClient);
        });        
        
        verify(clientPersistanceMock, times(0)).update(updatedClient);
    }
}
