package com.hexagonal.domain.services.client;

import org.junit.jupiter.api.Test;

import com.hexagonal.domain.Enums;
import com.hexagonal.domain.entites.Client;
import com.hexagonal.domain.exceptions.InvalidIdentifierException;
import com.hexagonal.domain.exceptions.ItemNotFoundException;
import com.hexagonal.domain.ports.input.client.IGetClientService;
import com.hexagonal.domain.ports.output.IClientPersistance;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class GetClientServiceTest {
    
    private IGetClientService getClientService;
    private IClientPersistance clientPersistanceMock;
    
    public GetClientServiceTest() {
        clientPersistanceMock = mock(IClientPersistance.class);
        getClientService = new GetClientService(clientPersistanceMock);
    }

    // Test Methods

    @Test
    public void getTest() {
        // Arrange
        Long id = 1L;
        
        Client existingClient = new Client();
        existingClient.setFirstName("Roger");
        existingClient.setLastName("Peterson");
        existingClient.setAge(45L);
        existingClient.setCityOfBirth("NY");
        existingClient.setIdType(Enums.IdentificationType.DNI);
        existingClient.setIdNumber("685643453");
        
        when(clientPersistanceMock.get(id)).thenReturn(existingClient);
        
        // Act
        Client client = getClientService.get(id);
        
        // Assert
        assertEquals(existingClient, client);
    }
    
    @Test
    public void getWithInvalidIdentifierTest() {
        // Arrange
        Long id = 0L;
        
        // Act and Assert
        assertThrows(InvalidIdentifierException.class, () -> {
            getClientService.get(id);
        });        
    }
    
    @Test
    public void getItemNotFoundTest() {
        // Arrange
        Long id = 1000L;
        Client existingClient = null;
        when(clientPersistanceMock.get(id)).thenReturn(existingClient);
        
        // Act and Assert
        assertThrows(ItemNotFoundException.class, () -> {
            getClientService.get(id);
        });        
    }
}
