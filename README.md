# Clean Architecture Challenge Java



## Getting started

- Clone or download the repository contents and open the **client** named folder to explore the Spring Java project created using Hexagonal Architecture.

- Restore the databases backups included in the **Databases BKs** folder (A MySQL database and a MongoDB collection).

- Run the Spring project using you favorite Java IDE.

- Run the unit tests to automatically generate the coverage report under a path named "[Project Path]\build\reports\jacoco\test\html" (The coverage was configured to evaluate only the com/hexagonal/domain/services classes that contains the business rules).


## How to test the API functionality

Test the API using Postman with the following url endpoints:

Get a client: **GET** http://localhost:8090/client/{client-id}<br>
Create a client: **POST** http://localhost:8090/client<br>
List/Search clients: **POST** http://localhost:8090/client/list<br>
Update a client: **PUT** http://localhost:8090/client/{client-id}<br>
Delete a client: **DELETE** http://localhost:8090/client/{client-id}<br>

For more information about the use and parameters of the services, please execute de **OpenAPI (Swagger)** documentation using the following url: http://localhost:8090/swagger-ui/index.html
